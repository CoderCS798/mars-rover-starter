package com.afs.tdd;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Objects;

public class MarsRover {
    public Location location;
    public MarsRover(Location initialLocation) {
        this.location = initialLocation;
    }

    public Location executeCommand(Command commandMove) {
        if(commandMove == Command.move){
            if(location.getDirection()==Direction.N){
                location.setY(location.getY()+1);
            }
            else if(location.getDirection()==Direction.S){
                location.setY(location.getY()-1);
            }
            else if(location.getDirection()==Direction.E){
                location.setX(location.getX()+1);
            }
            else if(location.getDirection()==Direction.W){
                location.setX(location.getX()-1);
            }
        }else if(commandMove == Command.turnLeft){
            if(location.getDirection()==Direction.N){
                location.setDirection(Direction.W);
            }
            else if(location.getDirection()==Direction.S){
                location.setDirection(Direction.E);
            }
            else if(location.getDirection()==Direction.E){
                location.setDirection(Direction.N);
            }
            else if(location.getDirection()==Direction.W){
                location.setDirection(Direction.S);
            }
        } else if(commandMove == Command.turnRight){
            if(location.getDirection()==Direction.N){
                location.setDirection(Direction.E);
            }
            else if(location.getDirection()==Direction.S){
                location.setDirection(Direction.W);
            }
            else if(location.getDirection()==Direction.E){
                location.setDirection(Direction.S);
            }
            else if(location.getDirection()==Direction.W){
                location.setDirection(Direction.N);
            }
        }
        return location;
    }
    public Location executeGroupCommand(String commandGroup){

        String[] commandArr = commandGroup.split("");
        System.out.println(Arrays.toString(commandArr));
        for (String s : commandArr) {
            if (Objects.equals(s, "M")) {
                executeCommand(Command.move);

            }
            else if (Objects.equals(s, "L")) {
                executeCommand(Command.turnLeft);
            }
           else if (Objects.equals(s, "R")) {
                executeCommand(Command.turnRight);
            }
        }

        return location;
    }
}
