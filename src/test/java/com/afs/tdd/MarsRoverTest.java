package com.afs.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MarsRoverTest {
    @Test
    void should_change_to_0_1_N_when_executeCommand_given_location_0_0_N_and_command_move(){
        Location initialLocation = new Location(0,0,Direction.N);
        MarsRover marsRover = new MarsRover(initialLocation);
        Command commandMove = Command.move;
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        Assertions.assertEquals(0,locationAfterMove.getX());
        Assertions.assertEquals(1,locationAfterMove.getY());
        Assertions.assertEquals(Direction.N,locationAfterMove.getDirection());
    }
    @Test
    void should_change_to_0_2_S_when_executeCommand_given_location_0_3_S_and_command_move(){
        Location initialLocation = new Location(0,3,Direction.S);
        MarsRover marsRover = new MarsRover(initialLocation);
        Command commandMove = Command.move;
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        Assertions.assertEquals(0,locationAfterMove.getX());
        Assertions.assertEquals(2,locationAfterMove.getY());
        Assertions.assertEquals(Direction.S,locationAfterMove.getDirection());
    }
    @Test
    void should_change_to_1_0_E_when_executeCommand_given_location_0_0_E_and_command_move(){
        Location initialLocation = new Location(0,0,Direction.E);
        MarsRover marsRover = new MarsRover(initialLocation);
        Command commandMove = Command.move;
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        Assertions.assertEquals(1,locationAfterMove.getX());
        Assertions.assertEquals(0,locationAfterMove.getY());
        Assertions.assertEquals(Direction.E,locationAfterMove.getDirection());
    }
    @Test
    void should_change_to_0_0_W_when_executeCommand_given_location_1_0_W_and_command_move(){
        Location initialLocation = new Location(1,0,Direction.W);
        MarsRover marsRover = new MarsRover(initialLocation);
        Command commandMove = Command.move;
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        Assertions.assertEquals(0,locationAfterMove.getX());
        Assertions.assertEquals(0,locationAfterMove.getY());
        Assertions.assertEquals(Direction.W,locationAfterMove.getDirection());
    }
    @Test
    void should_change_to_0_0_W_when_executeCommand_given_location_0_0_N_and_command_turnleft(){
        Location initialLocation = new Location(0,0,Direction.N);
        MarsRover marsRover = new MarsRover(initialLocation);
        Command commandMove = Command.turnLeft;
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        Assertions.assertEquals(0,locationAfterMove.getX());
        Assertions.assertEquals(0,locationAfterMove.getY());
        Assertions.assertEquals(Direction.W,locationAfterMove.getDirection());
    }
    @Test
    void should_change_to_0_0_E_when_executeCommand_given_location_0_0_S_and_command_turnleft(){
        Location initialLocation = new Location(0,0,Direction.S);
        MarsRover marsRover = new MarsRover(initialLocation);
        Command commandMove = Command.turnLeft;
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        Assertions.assertEquals(0,locationAfterMove.getX());
        Assertions.assertEquals(0,locationAfterMove.getY());
        Assertions.assertEquals(Direction.E,locationAfterMove.getDirection());
    }
    @Test
    void should_change_to_0_0_N_when_executeCommand_given_location_0_0_E_and_command_turnleft(){
        Location initialLocation = new Location(0,0,Direction.E);
        MarsRover marsRover = new MarsRover(initialLocation);
        Command commandMove = Command.turnLeft;
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        Assertions.assertEquals(0,locationAfterMove.getX());
        Assertions.assertEquals(0,locationAfterMove.getY());
        Assertions.assertEquals(Direction.N,locationAfterMove.getDirection());
    }
    @Test
    void should_change_to_0_0_S_when_executeCommand_given_location_0_0_W_and_command_turnleft(){
        Location initialLocation = new Location(0,0,Direction.W);
        MarsRover marsRover = new MarsRover(initialLocation);
        Command commandMove = Command.turnLeft;
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        Assertions.assertEquals(0,locationAfterMove.getX());
        Assertions.assertEquals(0,locationAfterMove.getY());
        Assertions.assertEquals(Direction.S,locationAfterMove.getDirection());
    }
    @Test
    void should_change_to_0_0_E_when_executeCommand_given_location_0_0_N_and_command_turnright(){
        Location initialLocation = new Location(0,0,Direction.N);
        MarsRover marsRover = new MarsRover(initialLocation);
        Command commandMove = Command.turnRight;
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        Assertions.assertEquals(0,locationAfterMove.getX());
        Assertions.assertEquals(0,locationAfterMove.getY());
        Assertions.assertEquals(Direction.E,locationAfterMove.getDirection());
    }
    @Test
    void should_change_to_0_0_W_when_executeCommand_given_location_0_0_S_and_command_turnright(){
        Location initialLocation = new Location(0,0,Direction.S);
        MarsRover marsRover = new MarsRover(initialLocation);
        Command commandMove = Command.turnRight;
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        Assertions.assertEquals(0,locationAfterMove.getX());
        Assertions.assertEquals(0,locationAfterMove.getY());
        Assertions.assertEquals(Direction.W,locationAfterMove.getDirection());
    }
    @Test
    void should_change_to_0_0_S_when_executeCommand_given_location_0_0_E_and_command_turnright(){
        Location initialLocation = new Location(0,0,Direction.E);
        MarsRover marsRover = new MarsRover(initialLocation);
        Command commandMove = Command.turnRight;
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        Assertions.assertEquals(0,locationAfterMove.getX());
        Assertions.assertEquals(0,locationAfterMove.getY());
        Assertions.assertEquals(Direction.S,locationAfterMove.getDirection());
    }
    @Test
    void should_change_to_0_0_N_when_executeCommand_given_location_0_0_W_and_command_turnright(){
        Location initialLocation = new Location(0,0,Direction.W);
        MarsRover marsRover = new MarsRover(initialLocation);
        Command commandMove = Command.turnRight;
        Location locationAfterMove = marsRover.executeCommand(commandMove);
        Assertions.assertEquals(0,locationAfterMove.getX());
        Assertions.assertEquals(0,locationAfterMove.getY());
        Assertions.assertEquals(Direction.N,locationAfterMove.getDirection());
    }
    @Test
    void should_change_to_0_1_N_when_executeCommand_given_location_1_0_N_and_command_MLMR(){
        Location initialLocation = new Location(1,0,Direction.N);
        MarsRover marsRover = new MarsRover(initialLocation);

        Location locationAfterMove = marsRover.executeGroupCommand("MLMR");
        Assertions.assertEquals(0,locationAfterMove.getX());
        Assertions.assertEquals(1,locationAfterMove.getY());
        Assertions.assertEquals(Direction.N,locationAfterMove.getDirection());
    }
}
